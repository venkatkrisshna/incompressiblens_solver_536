%IncompressibleNS_solver is a 2D incompressible solution to the
%Navier-Stokes equations using a staggered grid approach. It takes in a
%number of controlling arguments and returns the x and y velocities (U,V)
%and pressure field (P) as matrices
clear; clc; close all

% Simulation case
% ---------------
BC = 1; % boundary condition switch

% Simulation parameters
% ---------------------
L = 1.0;             % length in the x and y directions
n = 120;             % x-divisions
m = 120;             % y-divisions
U_dim = 1.0;         % non-dimensional x-velocity
tau = 0.5;           % stability factor for timestep change
t_end = 35;          % end time
step_end = 9999999;   % Maximum number of steps
outfreq = 10;        % Frequency of output generation

% Fluid properties
% ----------------
mu = 1;              % dynamic viscosity
rho = 1000;          % density
Re = rho*U_dim*L/mu; % Reynolds number

% Pressure solution parameters
% ----------------------------
omega = 1.7;         % system of overrelaxation step
maxiter = 1000;      % Maximum Pressure iterations
err = 1e-3;          % Pressure convergence criterion

% Setup domain
% ------------
dx = L/n;
dy = L/m;
U = zeros(n+1,m+2);  % x-velocity
V = zeros(n+2,m+1);  % y-velocity
P = zeros(n+2,m+2);  % pressure
x = (0:dx:1-dx) + 0.5*dx;
y = (0:dy:1-dy)'+ 0.5*dy;
[X,Y] = meshgrid(x,y);
y = flipud(y);
Y = flipud(Y);
u = zeros(n,m);
v = zeros(n,m);
    
% Initialize solution
% -------------------
t = 0;               % initial time
step = 0;            % initial timestep

% Movie init
% ----------
mov = VideoWriter('vel_movie.mp4', 'MPEG-4');
mov.FrameRate = 24;
mov.Quality = 85;
open(mov);
h = figure(1); clf(h)
title(['Velocity at t = ', num2str(t),'s after ', num2str(step),' steps']);
xlim([0 1]);
ylim([0 1]);
set(h, 'nextplot', 'replacechildren');
frame = getframe(h);
writeVideo(mov, frame);

% Solution init
% -------------
while(t <= t_end && step <= step_end)

    % COMPUTE TIMESTEP
    dt = comp_delt(U, V, Re, dx, dy, n, m, tau);

    % IMPOSE BOUNDARY CONDITIONS
    [U, V] = placeBCs(U, V, n, m, U_dim, BC);

    % PREDICTOR STEP
    [F, G] = solveFG(U, V, n, m, dx, dy, dt, mu, rho);

    % SOLVE PRESSURE POISSON
    [P, piter,maxres] = solvePoisson(P, F, G, rho, n, m, dx, dy, dt, omega, err, maxiter);

    % CORRECTOR STEP
    [U, V] = solveVel(U, V, F, G, P, rho, n, m, dx, dy, dt);

    % UPDATE MOVIE
    if mod(step, outfreq) == 0
        figure(gcf);
        u = (U(2:m+1,1:n) + U(2:m+1,2:n+1))/2.;
        v = (V(1:m,2:n+1) + V(2:m+1,2:n+1))/2.;
        h = quiver(X,Y,u,v);
        set(h,'LineWidth',2);
        xlim([0 1]);
        ylim([0 1]);
        title(['Velocity at t = ', num2str(t),'s after ', num2str(step),' steps']);
        frame = getframe(gcf);
        writeVideo(mov, frame);
        plot(linspace(0,1,length(V(:,(m)/2))),V(:,(m)/2))
        plot(U((n)/2,:),linspace(0,1,length(U((n)/2,:))))
    end
    
%     postProc(U, V, P, n, m, dx, dy);

    % UPDATE TIME AND STEP
    t = t + dt;
    step = step + 1;
    
end

% Post-processing
% ---------------
postProc(U, V, P, n, m, dx, dy);
close(mov);