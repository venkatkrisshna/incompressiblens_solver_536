function postProc(U, V, P, n, m, dx, dy)
%postProc takes the matrices of velocity (U,V) and pressure (P) and creates any desired plots
% n,m are the number of cells in the x and y directions
%
% dx,dy are the step sizes in the x and y directions

    U = rot90(U,1);
    V = rot90(V,1);
    P = rot90(P,1); % already at cell centers
    
    u = zeros(n,m);
    v = zeros(n,m);
    
    % organize position vectors
    x = (0:dx:1-dx) + 0.5*dx;
    y = (0:dy:1-dy)'+ 0.5*dy;
    [X,Y] = meshgrid(x,y);
    y = flipud(y);
    Y = flipud(Y);

    % interpolate velocities on center of grid
    u = (U(2:m+1,1:n) + U(2:m+1,2:n+1))/2.;
    v = (V(1:m,2:n+1) + V(2:m+1,2:n+1))/2.;
    
    % plot velocity as a vector
    figure(4);
    h = quiver(X,Y,u,v);
    title('Velocity');
    set(h,'LineWidth',2)

    % plot velocity magnitudes
    figure(3)
    surf(X,Y,u,v)

%     % plot streamslices
%     figure(4)
%     streamslice(???)

%     plot pressure
    figure(5);
    surf(X,Y,P(2:m+1, 2:n+1));
    title('Pressure');
end

