function [F, G] = solveFG(U, V, n, m, dx, dy, dt, mu, rho)
%solveFG solves for the predicted x and y velocities (U,V) given the
%current conditions. The predicted x and y velocities (F,G) are returned.

F = zeros(n+1,m); % x-momentum component
G = zeros(n,m+1); % y-momentum component

dxi = 1/dx; % division is more expensive than multiplication
dyi = 1/dy;
dxi2 = dxi*dxi; % two multiplications are more expensive than one
dyi2 = dyi*dyi;
nu = mu/rho;

    for i = 2:n
        for j = 2:m+1
            d2udx2 = dxi2*(U(i-1,j) - 2*U(i,j) + U(i+1,j)); % second derivative of u in x
            d2udy2 = dyi2*(U(i,j-1) - 2*U(i,j) + U(i,j+1)); % second derivative of u in y
            du2dx = dxi*(((U(i,j)+U(i+1,j))*0.5)^2-((U(i-1,j)+U(i,j))*0.5)^2); % first derivative of u^2 in x
            duvdy = dyi*(0.5*(U(i,j)+U(i,j+1))*0.5*(V(i,j)+V(i+1,j)) - 0.5*(U(i,j-1)+U(i,j))*0.5*(V(i,j-1)+V(i+1,j-1))); % first derivative of uv in y
            
            F(i,j) = U(i,j) + dt*(nu*(d2udx2 + d2udy2) - du2dx - duvdy); % predict the updated u velocity
        end
    end

    for i = 2:n+1
        for j = 2:m
            d2vdx2 = dxi2*(V(i-1,j) - 2*V(i,j) + V(i+1,j)); % second derivative of v in x
            d2vdy2 = dyi2*(V(i,j-1) - 2*V(i,j) + V(i,j+1)); % second derivative of v in y
            dvudx = dxi*(0.5*(U(i,j)+U(i,j+1))*0.5*(V(i,j)+V(i+1,j)) - 0.5*(U(i-1,j)+U(i-1,j+1))*0.5*(V(i-1,j)+V(i,j))); % first derivative of vu in x
            dv2dy = dyi*(((V(i,j)+V(i,j+1))*0.5)^2-((V(i,j-1)+V(i,j))*0.5)^2); % first derivative of v^2 in y
            
            G(i,j) = V(i,j) + dt*(nu*(d2vdx2 + d2vdy2) - dvudx - dv2dy); % predict the updated v velocity
        end
    end
    
    % update the boundary cells of F and G, having arbitrarily chosen the velocities
    F(1,2:m+1) = U(1,2:m+1);
    F(n+1,2:m+1) = U(n+1,2:m+1);
    G(2:n+1,1) = V(2:n+1,1);
    G(2:n+1,m+1) = V(2:n+1,m+1);
end

