function [U, V] = solveVel(U, V, F, G, P, rho, n, m, dx, dy, dt)
%solveVel performs the corrector step and updates the velocities to the
%next time-step. The x and y velocities (U,V) are returned.

rhoi = 1/rho;
dxi = 1/dx;
dyi = 1/dy;

for i = 2:n
    for j = 2:m+1
        U(i,j) = F(i,j) - dt*rhoi*dxi*(P(i+1,j) - P(i,j));
    end
end
for i = 2:n+1
    for j = 2:m
        V(i,j) = G(i,j) - dt*rhoi*dyi*(P(i,j+1) - P(i,j));
    end
end

end

