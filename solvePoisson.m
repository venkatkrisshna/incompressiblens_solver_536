function [P, piter,maxres] = solvePoisson(P, F, G, rho, n, m, dx, dy, dt, omega, err, maxiter)
%solvePoisson solves the Poisson equation for the pressure field at the
%next time-step. The pressure (P) and divergence (divg) are returned.

%NOTE: The previous pressure values (P) could be used as better
%initial guesses to speed up convergence

rhs = zeros(n+2,m+2); % rhs
res_ij = zeros(n+2,m+2); % rhs

dti = 1/dt; % invert the time-step this once because multiplications are faster
dxi = 1/dx;
dyi = 1/dy;
dxi2 = dxi*dxi; % two multiplications are more expensive than one
dyi2 = dyi*dyi;

for i = 2:n+1
    for j = 2:m+1
        rhs(i,j) = rho*dti*(dxi*(F(i,j)-F(i-1,j)) + dyi*(G(i,j)-G(i,j-1)));
    end
end

% fprintf('Iter     Residual\n');
% fprintf('--------------------\n');
% reset the values and begin iterating
piter = 0;
maxres = 1e+10;
while maxres > err && piter<maxiter
    for j = 2:m+1
        for i = 2:n+1
            P(i,j) = (1-omega)*P(i,j) + omega*((2.*dxi2 + 2.*dyi2)^-1)*(dxi2*(P(i-1,j) + P(i+1,j)) + dyi2*(P(i,j-1) + P(i,j+1)) - rhs(i,j));
        end
    end
    
    % update the pressure boundary conditions in the ghost cells
    P(1  ,2:m+1) = P(2,2:m+1);
    P(n+2,2:m+1) = P(n+1,2:m+1);
    P(2:n+1,1  ) = P(2:n+1,2);
    P(2:n+1,m+2) = P(2:n+1,m+1);
    
    % check the divergence
    maxres = -1e+10;
    for i = 2:n+1
        for j = 2:m+1
            res_ij(i-1,j-1) = dxi2*(P(i-1,j)-2*P(i,j)+P(i+1,j)) + dyi2*(P(i,j-1)-2*P(i,j)+P(i,j+1)) - rhs(i,j);
        end
    end
%     maxres = max(max(res_ij)); % resMAX
    maxres = sqrt(1/(n*m) * sum(sum(res_ij.^2))); % resRMS

    % update the iterator
    piter = piter + 1;
end

end