function [U,V] = placeBCs(U, V, n, m, U_dim, BC)
%placeBCs places the boundary conditions for the defined problem. The x and
%y velocities (U,V) are returned after having the boundary conditions updated.

% BC defines the problem
%    1 solves a lid-driven cavity flow
%    2 solves for pipe-flow in the x directions

    switch BC
        case 1 % lid-driven, cavity flow
            % i == 1, left wall
                V(1,2:m+1) = -V(2,2:m+1); % stationary
                U(1,2:m+1) = 0; % no permeation
            % i == n+1, right wall
                U(n+1,2:m+1) = 0; % no permeation
            % i == n+2, right wall
                V(n+2,2:m+1) = -V(n+1,2:m+1); % stationary

            % j == 1, bottom wall
                U(2:n+1,1) = -U(2:n+1,2); % stationary
                V(2:n+1,1) = 0; % no permeation
            % j == m+1, top wall
                V(2:n+1,m+1) = 0; % no permeation
            % j == m+2, top wall
                U(2:n+1,m+2) = 2*U_dim - U(2:n+1,m+1); % moving in the x-direction

        case 2 % pipe flow in x-direction
            % i == 1, left wall
                V(1,2:m+1) = -V(2,2:m+1); % inflow
                U(1,2:m+1) = U_dim*ones(size(U(1,2:m+1))); % inflow
            % i == n+1, right wall
                U(n+1,2:m+1) = U(n,2:m+1); % outflow
            % i == n+2, right wall
                V(n+2,2:m+1) = V(n+1,2:m+1); % outflow

            % j == 1, bottom wall
                U(2:n+1,1) = -U(2:n+1,2); % stationary
                V(2:n+1,1) = 0; % no permeation
            % j == m+1, top wall
                V(2:n+1,m+1) = 0; % no permeation
            % j == m+2, top wall
                U(2:n+1,m+2) = -U(2:n+1,m+1); % stationary
    end
end

