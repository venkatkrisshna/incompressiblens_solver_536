function [dt] = comp_delt(U, V, Re, dx, dy, n, m, tau)
%comp_delt calculates the next time-step using the Courant-Friedrichs-Lewy
%(CFL) condition. If the CFL number drops below 1e-9, it is assumed to have 
%reached a lower limit (e.g. starting from a zero velocity), though this may 
%impact stability. The time-step is returned.

dxi = 1/dx;
dyi = 1/dy;

dt = tau*max(1e-9, min([0.5*Re/(dxi^2 + dyi^2), dx/max(max(abs(U))), dy/max(max(abs(V)))]));

end

